module Client

open Elmish
open Elmish.React

open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fable.PowerPack.Fetch
open Fable.Recharts
open Fable.Recharts.Props

open Shared

open Fulma
open Fable.FontAwesome
open Fulma.Elmish
open Fable.Helpers.Isomorphic
open Thoth.Json

module URI =
    open Fable.Import

    let private getURIhash(): string  = Browser.window.location.hash.TrimStart([|'?';'#'|])
    let private setURIhash(x: string): unit = Browser.window.location.hash <- (if x="" then "" else "?" + x)

    let parseQuery() =
        getURIhash().Split[|'&'|]
        |> Seq.choose (fun s ->
            match s.Split[|'='|] |> Seq.toList with
            | [key; value] -> Some (key, value)
            | _ -> None)
        |> Map.ofSeq

    let updateQuery m =
        m |> Map.toSeq |> Seq.map (fun (key, value) -> key + "=" + value) |> String.concat "&"
        |> setURIhash


type DateTime = System.DateTime

type Components = {
    FromDatePicker : DatePicker.Types.State
    ToDatePicker : DatePicker.Types.State }

type ClientModel = {
    Model : Model
    Components : Components
}

type Msg =
| Init of Result<Model, exn>
| SelectStation of string
| Back
| FromDateChanged of DatePicker.Types.State * (DateTime option)
| ToDateChanged of DatePicker.Types.State * (DateTime option)

let datePickerState() = { DatePicker.Types.defaultState with AutoClose = true; ShowDeleteButton = false }

let init () : ClientModel * Cmd<Msg> =
    let model = {
        Model = Model.Loading
        Components = { FromDatePicker = datePickerState(); ToDatePicker = datePickerState() } }
    let cmd =
        Cmd.ofPromise
            (fetchAs<_> "/api/chmu-air/init" (Decode.Auto.generateDecoder()))
            []
            (Ok >> Init)
            (Error >> Init)
    let cmd = URI.parseQuery() |> Map.tryFind "s" |> Option.map (SelectStation >> Cmd.ofMsg) |> Option.defaultValue cmd
    model, cmd

let station x (fromDate: DateTime option) (toDate: DateTime option) clientModel : ClientModel * Cmd<Msg> =
    let fromDate = fromDate |> Option.defaultValue (System.DateTime.Now.AddDays(-14.))
    let toDate = toDate |> Option.defaultValue (System.DateTime.Now.AddDays(1.0))
    let model = Model.Loading
    let cmd =
        Cmd.ofPromise
            (fetchAs<_> ("/api/chmu-air/station/" + x + "/" + fromDate.ToString("yyyyMMdd") + "/" + toDate.ToString("yyyyMMdd")) <| Decode.Auto.generateDecoder())
            []
            (Ok >> Init)
            (Error >> Init)
    let pickerSetReferenceDate (p: DatePicker.Types.State) d = { p with ReferenceDate = d}
    let components =
        { clientModel.Components with
            FromDatePicker = pickerSetReferenceDate clientModel.Components.FromDatePicker fromDate
            ToDatePicker = pickerSetReferenceDate clientModel.Components.ToDatePicker toDate
            }
    {Model = model; Components = components}, cmd

let update (msg : Msg) (model : ClientModel) : ClientModel * Cmd<Msg> =
    let lift (m, c) = { model with Model = m }, c
    let updateDateWidget componentUpdateF (m,c) =
            { model with
                Model = m.Model
                Components = componentUpdateF model.Components }, c
    match model.Model, msg with
    | _, Back ->
        URI.updateQuery (Map.ofSeq [])
        init()
    | Model.Loading, Init (Ok x) -> lift (x, Cmd.none)
    | _, Init (Error x) -> lift (Model.Error (x.Message), Cmd.none)
    | _, SelectStation x ->
        URI.updateQuery (Map.ofSeq ["s", x])
        station x None None model
    | Model.Data (s,fromDate,toDate,_), FromDateChanged (newState, date) ->
        // Store the new state and the selected date
        if date = Some fromDate then model, Cmd.none else station s date (Some toDate) model
        |> updateDateWidget (fun c -> { c with FromDatePicker = newState })
    | Model.Data (s,fromDate,toDate,_), ToDateChanged (newState, date) ->
        // Store the new state and the selected date
        if date = Some toDate then model, Cmd.none else station s (Some fromDate) date model
        |> updateDateWidget (fun c -> { c with ToDatePicker = newState })
    | _, _ ->
        model, Cmd.none

let footer =
    let intersperse sep ls =
        List.foldBack (fun x -> function
            | [] -> [x]
            | xs -> x::sep::xs) ls []

    let components =
        [
            "Saturn", "https://saturnframework.github.io/docs/"
            "Fable", "http://fable.io"
            "Elmish", "https://elmish.github.io/elmish/"
            "Fulma", "https://mangelmaxime.github.io/Fulma"
        ]
        |> List.map (fun (desc,link) -> a [ Href link ] [ str desc ] )
        |> intersperse (str ", ")
        |> span [ ]

    div [] [
        p [ ]
            [ strong [] [ str "CHMU-air" ]
              str " powered by: "
              components ]
        p [ ]
            [ str "Developed by "
              a [ Href "https://jindraivanek.gitlab.io"] [ str "Jindřich Ivánek" ]
              str ", "
              a [ Href "https://gitlab.com/jindraivanek/chmu-air"] [ str "source code" ]
              ]
    ]

let show = function
| Some x -> string x
| None -> "Loading..."

let button txt onClick =
    Button.button
        [ Button.IsFullWidth
          Button.Color IsPrimary
          Button.OnClick onClick ]
        [ str txt ]

let dropDown label items activeItem =
    let menu = items |> Seq.map (fun x ->
        Dropdown.Item.a (if activeItem = x then [ Dropdown.Item.IsActive true ] else []) [ str x ]) |> Seq.toList
    Dropdown.dropdown [ Dropdown.IsHoverable ]
        [ div [ ]
            [ Button.button [ ]
                [ span [ ]
                    [ str label ]
                  Icon.icon [ Icon.Size IsSmall ] [ ] ] ]
          Dropdown.menu [ ]
            [ Dropdown.content [ ]
                menu ] ]

let columns xs =
    Columns.columns [] (xs |> List.map (fun x -> Column.column [] [ x ]))

let viewStations xs dispatch =
    [ Heading.h3 [] [ str ("Stations:")]
      table [] (xs |> List.map (fun x -> tr [] [td [] [button x (fun _ -> dispatch (SelectStation x))]])) ]
      //dropDown "Station" xs "" ]

let viewData model (station, fromDate, toDate, data) dispatch =
    let components = data |> List.map (fun x -> x.Component) |> List.distinct
    let chmuData =
        let header = (td [] [str "Time"]) :: (components |> List.map (fun x -> td [] [str x]))
        let body =
            data
            |> Seq.groupBy (fun a -> a.Date) |> Seq.sortByDescending fst
            |> Seq.map (fun (d, xs) ->
                let m = xs |> Seq.map (fun a -> a.Component, a) |> Map.ofSeq
                [ td [] [str (sprintf "%A" d)] ]
                @ (components |> List.map (fun c ->
                    m |> Map.tryFind c |> Option.map (fun a -> b [] [sprintf "%.1f" a.Value |> str]) |> Option.defaultValue (str "")
                    |> fun x -> td [] [x])))
            |> Seq.toList |> List.map (fun x -> tr [] x)
        table [] ((tr [] header) :: body)

    let chart comp =
        let data = data |> List.filter (fun x -> x.Component = comp) |> List.toArray
        lineChart [
            Chart.Data data
            Chart.Width 1000.
            Chart.Height 500.
        ]
            [ xaxis [ Cartesian.Type "category"; Cartesian.DataKey "Date" ] []
              yaxis [ Cartesian.Type "number"; Cartesian.DataKey "Value" ] []
              //tooltip [] []
              line [ Cartesian.DataKey "Value" ] [] ]

    let picker cmd (state: DatePicker.Types.State) date =
        let pickerConfig : DatePicker.Types.Config<Msg> =
            let c = DatePicker.Types.defaultConfig cmd
            { c with DatePickerStyle = [Position "absolute"
                                        MaxWidth "450px"
                                         // Fix calendar display for Ipad browsers (Safari, Chrome)
                                        // See #56: https://github.com/MangelMaxime/Fulma/issues/56#issuecomment-332186559
                                        ZIndex 10. ] }
        DatePicker.View.root pickerConfig state (Some date) dispatch

    [ Heading.h3 [] [ str station ] ]
    @ [ columns [
        picker FromDateChanged model.Components.FromDatePicker fromDate
        picker ToDateChanged model.Components.ToDatePicker toDate ] ]
    @ (components |> List.collect (fun x -> [ Heading.h3 [] [str x]; chart x ]))
    @ [ chmuData ]


let viewModel (model : ClientModel) dispatch =
    match model.Model with
    | Model.Loading -> [ Heading.h3 [] [ str ("Loading...")] ]
    | Model.Error msg -> [ Heading.h3 [] [ str ("Error")]; str msg ]
    | Model.Stations xs -> viewStations xs dispatch
    | Model.Data (a,b,c,d) -> viewData model (a,b,c,d) dispatch

let view (model : ClientModel) (dispatch : Msg -> unit) =
    div []
        [ Navbar.navbar [ Navbar.Color IsPrimary ]
            [ Navbar.Item.div [ ]
                [ Heading.h2 [ ]
                    [ str "CHMU-air" ] ] ]

          Container.container []
              [
                Heading.h3 [] [ str "" ]
                Columns.columns []
                    [ Column.column [] [ button "back" (fun _ -> dispatch Back) ] ]
                Content.content [ Content.Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ]
                    (viewModel model dispatch)
              ]

          Footer.footer [ ]
                [ Content.content [ Content.Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ]
                    [ footer ] ] ]


#if DEBUG
open Elmish.Debug
open Elmish.HMR
#endif

Program.mkProgram init update view
#if DEBUG
|> Program.withConsoleTrace
//|> Program.withHMR
#endif
|> Program.withReact "elmish-app"
#if DEBUG
|> Program.withDebugger
#endif
|> Program.run

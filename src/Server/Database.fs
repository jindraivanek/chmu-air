module Database

open Dapper
open System.Data.Common
open System.Collections.Generic
open FSharp.Control.Tasks.ContextInsensitive

let inline (=>) k v = k, box v

let execute (connection:#DbConnection) (sql:string) (data:_) =
    task {
        try
            let! res = connection.ExecuteAsync(sql, data)
            return Ok res
        with
        | ex -> return Error ex
    }

let query (connection:#DbConnection) (sql:string) (parameters:IDictionary<string, obj> option) =
    task {
        try
            let res =
                match parameters with
                | Some p ->
                        connection.Query<'T>(sql, p) |> Seq.toArray
                | None -> connection.Query<'T>(sql) |> Seq.toArray
            //printfn "%A" (sql,parameters)
            //printfn "%A" res
            return Ok (Seq.toArray res)
        with
        | ex ->
            printfn "query error %A" ex.InnerException
            //return Error ex
            return Ok [||] //workaround https://github.com/StackExchange/Dapper/issues/848
    }

let querySingle (connection:#DbConnection) (sql:string) (parameters:IDictionary<string, obj> option) =
    task {
        try
            let! res =
                match parameters with
                | Some p -> connection.QuerySingleOrDefaultAsync<'T>(sql, p)
                | None -> connection.QuerySingleOrDefaultAsync<'T>(sql)
            return
                if isNull (box res) then Ok None
                else Ok (Some res)

        with
        | ex -> return Error ex
    }

module Server

open System.IO
open System.Threading.Tasks

open Microsoft.AspNetCore.Builder
open Microsoft.Extensions.DependencyInjection
open Giraffe
open Saturn
open Shared

open Giraffe.Serialization

open Config
open AirQuality
open FSharp.Control.Tasks


let publicPath = Path.GetFullPath "../Client/public"
let port = 8085us
let config = Config.Default

let getStations ctx : Task<Model> = task {
      let cnf = Controller.getConfig ctx
      let! result = AirQuality.Database.getStations cnf.connectionString
      match result with
      | Ok result ->
        return Model.Stations (Seq.toList result)
      | Error ex ->
        return raise ex
    }

let getData ctx station fromDate toDate : Task<Model> = task {
      let cnf = Controller.getConfig ctx
      let! result = AirQuality.Database.getData cnf.connectionString (Some station) (Some fromDate, Some toDate)
      match result with
      | Ok result ->
        return Model.Data (station, fromDate, toDate, result |> Seq.map (fun aq ->
            { Date = aq.Date.ToShortDateString() + " " + aq.Date.ToShortTimeString(); Component = aq.Component; Value = aq.Value }) |> Seq.toList)
      | Error ex ->
        return raise ex
    }


let webApp = router {
    get "/api/chmu-air/init" (fun next ctx ->
        task {
            let! model = getStations ctx
            return! Successful.OK model next ctx
        })
    getf "/api/chmu-air/station/%s/%s/%s" (fun (station, fromDateString, toDateString) next ctx ->
        let parseDate s = System.DateTime.ParseExact(s, "yyyyMMdd", null)
        task {
            let fromDate = parseDate fromDateString
            let toDate = parseDate toDateString
            let! model = getData ctx station fromDate toDate
            return! Successful.OK model next ctx
        })
}

let configureSerialization (services:IServiceCollection) =
    services.AddSingleton<Giraffe.Serialization.Json.IJsonSerializer>(Thoth.Json.Giraffe.ThothSerializer())


let app = application {
    url ("http://0.0.0.0:" + port.ToString() + "/")
    use_router webApp
    memory_cache
    use_static publicPath
    service_config configureSerialization
    use_gzip
    use_config (fun _ ->  config)
}

let periodicJobs cnf = [
        (0,0,10), (fun () ->
            printfn "%A DataLoader.insertValues started" System.DateTime.Now
            AirQuality.Database.insertValues cnf.connectionString
            |> Task.map (fun t -> Seq.map Result.logError)
            |> Task.WaitAll)
    ]

let rec runJob ((h,m,s), f) =
    let d = System.DateTime.Now
    let t = System.TimeSpan(0,h,m,s)
    let rem x y = if y>0 then y - (x%y) else 0
    let nextRun = System.TimeSpan(0, rem d.Hour h, rem d.Minute m, rem d.Second s)
    async {
        System.Threading.Thread.Sleep(d.Add(nextRun) - System.DateTime.Now)
        f()
        return! runJob ((h,m,s), f)
    }

periodicJobs config |> Seq.iter (fun j -> runJob j |> Async.Start)

Migrations.Program.main [||]

run app

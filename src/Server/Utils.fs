[<AutoOpen>]
module Utils

open System.Threading
open System.Threading.Tasks
open FSharp.Control.Tasks.ContextInsensitive

module Task =
    let map (f: 'a -> 'b) (t: System.Threading.Tasks.Task<'a>)  = task { 
        let! x = t
        return f x}

    let withTimeout (timeout : int) (t : Task<'T>) =
        task {
            use cts = new CancellationTokenSource()
            use timer = Task.Delay (timeout, cts.Token)
            let! completed = Async.AwaitTask <| Task.WhenAny(t, timer)
            if completed = (t :> Task) then
                cts.Cancel ()
                let! result = Async.AwaitTask t
                return Some result
            else return None
        }    

module Result =
    open System.Threading.Tasks
    open FSharp.Control.Tasks.ContextInsensitive
    let logError = function
        | Ok x -> Ok x
        | Error e -> printfn "%A" e; Error e

module TaskResult =
    let bind (f : 'a -> Task<Result<'b,'e>>) (t : Task<Result<'a,'e>>) = task {
    //let bind f t = task {
        let! t = t
        return!
            match t with
            | Ok x -> f x
            | Error e -> task { return (Error e) }
        }

    let collect (f : 'a -> Task<#seq<Result<'b,'e>>>) (t : Task<Result<'a,'e>>) = task {
    //let bind f t = task {
        let! t = t
        return!
            match t with
            | Ok x -> f x
            | Error e -> task { return Seq.singleton (Error e) }
        }    


let memoize (f: 'a -> 'b) =
    let d = System.Collections.Concurrent.ConcurrentDictionary<'a,'b>()
    fun x -> d.GetOrAdd(x, f)

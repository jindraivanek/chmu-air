namespace AirQuality
open Saturn.CSRF.View

module Fetch =
    open FSharp.Data
    open FSharp.Control.Tasks

    let [<Literal>]url = "http://portal.chmi.cz/files/portal/docs/uoco/web_generator/aqindex_cze.json"
    let [<Literal>]sampleUrl = "http://portal.chmi.cz/files/portal/docs/uoco/web_generator/aqindex_cze.json"
    type Aqindex = JsonProvider<sampleUrl, Encoding="UTF-8">
    let loadValues () = task {
        let parseDate (str:string) =
            str.Replace(" UTC", "") |> System.DateTime.Parse
        
        let data = Aqindex.Load(url)

        let cr = (data.States |> Seq.head)
        let values = 
            cr.Regions |> Seq.collect (fun r ->
                (r.Stations |> Seq.collect (fun s -> s.Components |> Seq.choose (fun c -> c.Val.Number |> Option.map (fun x -> 
                    { 
                        Date=parseDate cr.DateFromUtc
                        DateTo=parseDate cr.DateToUtc
                        Region=r.Name.Trim()
                        Station=s.Name.Trim()
                        Component=c.Code.Trim()
                        Interval=c.Int
                        Value=x
                    }))))
            )
        return values
        }
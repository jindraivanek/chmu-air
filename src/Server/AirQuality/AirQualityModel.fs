namespace AirQuality

open Dapper

[<CLIMutable>]
type AirQuality = {
    Date: System.DateTime
    DateTo: System.DateTime
    Region: string
    Station: string
    Component: string
    Interval: string
    Value: decimal
}

namespace AirQuality

open Database
open System.Threading.Tasks
open FSharp.Control.Tasks.ContextInsensitive
open System

module Database =
    use connection = memoize <| fun connectionString -> new Npgsql.NpgsqlConnection(connectionString)

    let sqlWhere conditions = "WHERE " + (conditions |> String.concat " AND ")

    let getData connectionString maybeStation (fromDate, toDate) : Task<Result<AirQuality array, exn>> =
        let lift n o = Option.map (fun x -> n, box x) o
        let sql =
            maybeStation |> Option.map (fun (s:string) ->
                "SELECT * FROM AQ " +
                ([
                    Some "Station = @Station"
                    fromDate |> Option.map (fun x -> "Date >= @FromDate")
                    toDate |> Option.map (fun x -> "Date <= @ToDate")
                ] |> Seq.choose id |> sqlWhere))
            |> Option.defaultWith (fun () -> "SELECT * FROM AQ")
        let o = Seq.choose id [ lift "Station" maybeStation; lift "FromDate" fromDate; lift "ToDate" toDate ] |> dict
        printfn "%A" (sql, o)
        //db.Query<AQ>(sql, o)

        query (connection connectionString) sql (Some o)

    let getStations : string -> Task<Result<string array, exn>> = memoize <| fun connectionString ->
        query (connection connectionString) "SELECT DISTINCT station FROM AQ" None

    let insertValues connectionString =
        let tryInsert (x: AirQuality) =
            task {
                //let! _ = Task.Delay(TimeSpan.FromMilliseconds(100.0))
                let p = dict [
                    "Component" => x.Component
                    "Date" => x.Date
                    "DateTo" => x.DateTo
                    "Interval" => x.Interval
                    "Station" => x.Station
                    "Region" => x.Region
                    "Value" => x.Value ]
                let! res = query (connection connectionString) "SELECT * FROM AQ WHERE Station=@Station AND Date=@Date AND Component=@Component" (Some p)
                match res with
                | Ok xs when Seq.isEmpty xs ->
                    //Ok (db.Insert<AQ>(x))
                    printfn "to insert %A" x
                    return! query (connection connectionString)
                        "INSERT INTO AQ(Component, Date, DateTo, Interval, Station, Region, Value) VALUES (@Component, @Date, @DateTo, @Interval, @Station, @Region, @Value)"
                        (Some p)
                | Ok _ -> return (Ok Array.empty)
                | Error e ->
                    return (Error e)
            }
            |> Task.withTimeout (60*1000)
            |> Task.map (Option.defaultValue (Error (TimeoutException(sprintf "Timeout when trying to insert %A" x) :> exn)))

        task {
            let! values =
                Fetch.loadValues() |> Task.map Ok |> Task.withTimeout (60*1000)
                |> Task.map (Option.defaultValue (Error (TimeoutException(sprintf "Timeout when loading values") :> exn)))
            let! x =
                let y = values |> Result.map (Seq.map (fun x -> tryInsert x) >> Seq.toArray >> Task.WhenAll) |> Task.FromResult
                y |> TaskResult.collect (Task.map Array.toSeq)
            return x

        }

namespace Migrations
open SimpleMigrations

[<Migration(201901271205L, "Create AirQuality")>]
type CreateAirQuality() =
  inherit Migration()

  override __.Up() =
    base.Execute(@"CREATE TABLE AQ(
        Date TIMESTAMP NOT NULL,
        DateTo TIMESTAMP NOT NULL,
        Region TEXT NOT NULL,
        Station TEXT NOT NULL,
        Component TEXT NOT NULL,
        Interval TEXT NOT NULL,
        Value decimal NOT NULL
    )")

  override __.Down() =
    base.Execute(@"DROP TABLE AQ")

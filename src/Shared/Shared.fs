namespace Shared

type Item = { Date: string; Component: string; Value: decimal }

[<RequireQualifiedAccess>]
type Model = 
    | Loading
    | Error of string
    | Stations of string list
    | Data of string * System.DateTime * System.DateTime * Item list

